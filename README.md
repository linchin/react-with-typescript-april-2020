## ReactJS with TypeScript

**Some Online Resources**

TypeScript in React Documentation
<https://reactjs.org/docs/static-type-checking.html#typescript 
NB use create-react-app but go through manually to see relevant pieces 

React & Webpack in the TypeScript documentation
<https://www.typescriptlang.org/docs/handbook/react-&-webpack.html

Adding TypeScript in the Create-React-App documentation
<https://create-react-app.dev/docs/adding-typescript/

Runtime type checking for React props and similar objects
<https://www.npmjs.com/package/prop-types

Bootstrap a TypeScript + React project
Goes through every line of typescript configuration
<https://github.com/basarat/typescript-react/tree/master/01%20bootstrap

JSX in the TypeScript documentation
<https://www.typescriptlang.org/docs/handbook/jsx.html

TypeScript React Conversion Guide
Microsofts own tutorial - convert a fully js app to a fully ts app
<https://github.com/Microsoft/TypeScript-React-Conversion-Guide

Using TypeScript with React (Alligator tutorial)
<https://alligator.io/react/typescript-with-react/

React+TypeScript Cheatsheets
<https://github.com/typescript-cheatsheets/react-typescript-cheatsheet#reacttypescript-cheatsheets

Understanding TypeScript’s type notation
<https://2ality.com/2018/04/type-notation-typescript.html

TypeScript in 5 minutes
<https://www.typescriptlang.org/docs/handbook/typescript-in-5-minutes.html
